export const environment = {
  BASE_SERVER_URL: 'http://localhost:1337/',
  BASE_URL: 'http://localhost:5000/',
  COMPANY_NAME: 'Wink S.r.l.',
  APP_NAME: 'Winkular',
  ANALYTICS_UA_ID: '',
  DEFAULT_LANGUAGE: 'en',
  CURRENCY: '€',
  isFirebase: false,
  production: true
};
